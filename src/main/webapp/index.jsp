<%@ page language="java" contentType="text/html; charset=UTF-8"   isELIgnored="false"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Employee rooster</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="resources/css/screen.css" />
<head>
<body>
    <div id="container">
        <div id="content">

            <%@ include file="registrationForm.jsp"%>

            <%@ include file="registrationResult.jsp"%>

        </div>
        <div id="footer">
        </div>
    </div>
</body>
</html>
