<%@ page language="java" contentType="text/html; charset=UTF-8"   isELIgnored="false"
         pageEncoding="ISO-8859-1"%>
<html>
<head>
    <title>Login Error</title>
</head>
<body>
<c:url var="url" value="/index.jsp"/>
<h2>Invalid user name or password.</h2>

<p>Please enter a user name or password that is authorized to access this
    application.

    Click here to <a href="/jboss-employee-rooster-jsp">Try Again</a></p>
</body>
</html>